<!--
SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact@peren.gouv.fr>

SPDX-License-Identifier: MIT
-->

<!--
Project changelog.
This file is required if you want to auto-publish packages with CI and git tags.

To publish a new version, create a new section with the following format:

## v<version_number> (<tag_commit_date>)

Remarks:
- `version_number` should be a valid python package version
- `tag_commit_date` should be in ISO (YYYY-MM-DD) format.
Concrete example:

## v0.1.2 (2020-08-31)
-->

# Changelog
## v0.4.1 (2025-02-18)
Fix broken package with cuda-python >= 11.8.6

## v0.4.0 (2025-01-21)
- Add the `cpu_fallback` option to autoconfig parameters to use CPU when GPU is not available
- Add `PytorchCpuConfigurator` and `TensorflowCpuConfigurator`
- Rename old configurators to `PytorchGpuConfigurator` and `TensorflowGpuConfigurator`

## v0.3.4 (2024-12-09)
Internal changes:
- Add typing with mypi
- Fix incorrect types and signatures
- Add pre-commit hooks

## v0.3.3 (2024-10-01)
- Fix GPU enumeration
- Add new "free" strategy
- Add a method to bypass "CUDA_VISIBLE_DEVICES"
- Update README with a strategy section

# SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

from enum import Enum
from typing import NewType

GpuCudaId = NewType("GpuCudaId", int)
GpuPciId = NewType("GpuPciId", str)


class AllocationStrategies(str, Enum):
    memory = "memory"
    """GPU with most free memory"""
    load = "load"
    """least loaded GPU"""
    free = "free"
    """first totally free GPU"""

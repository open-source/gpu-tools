# SPDX-FileCopyrightText: 2024 Pôle d'Expertise de la Régulation Numérique <contact.peren@finances.gouv.fr>
#
# SPDX-License-Identifier: MIT

from typing import Any

from cuda.bindings import nvrtc
from cuda.bindings import driver as cuda_driver

from .types import GpuCudaId, GpuPciId


# The two following functions are adapted from the cuda-python documentation
# https://nvidia.github.io/cuda-python/overview.html#cuda-python-workflow


def _cudaGetErrorEnum(error: cuda_driver.CUresult | nvrtc.nvrtcResult) -> str:
    if isinstance(error, cuda_driver.CUresult):
        err, name = cuda_driver.cuGetErrorName(error)
        return name if err == cuda_driver.CUresult.CUDA_SUCCESS else "<unknown>"
    elif isinstance(error, nvrtc.nvrtcResult):
        return nvrtc.nvrtcGetErrorString(error)[1]  # type: ignore
    else:
        raise RuntimeError("Unknown error type: {}".format(error))


def _cu_check(result: Any) -> Any:
    if result[0].value:
        raise RuntimeError("CUDA error code={}({})".format(result[0].value, _cudaGetErrorEnum(result[0])))
    if len(result) == 1:
        return None
    elif len(result) == 2:
        return result[1]
    else:
        return result[1:]


_is_cuda_initialized = False


def _cuda_init_once() -> None:
    # The documentation does not describe what happen if you use cuInit multiple times.
    # In practice I did not see any side effects, but better safe than sorry.
    # https://docs.nvidia.com/cuda/cuda-driver-api/group__CUDA__INITIALIZE.html
    global _is_cuda_initialized  # pylint: disable=global-statement
    if _is_cuda_initialized:
        return
    _cu_check(cuda_driver.cuInit(0))
    _is_cuda_initialized = True


def get_devices_pci_id() -> dict[GpuCudaId, GpuPciId]:
    _cuda_init_once()

    def get_pci_id(device_number: int) -> str:
        PCI_STR_BUFFER_SIZE = 13  # See https://docs.nvidia.com/cuda/cuda-driver-api/group__CUDA__MEM.html
        device_handle = _cu_check(cuda_driver.cuDeviceGet(device_number))
        raw_id: bytes = _cu_check(cuda_driver.cuDeviceGetPCIBusId(PCI_STR_BUFFER_SIZE, device_handle))
        str_id = raw_id.rstrip(b"\x00").decode("ascii")
        # pci id normalization `0000:01:00.0` -> `00000000:01:00.0`
        domain_part, *others = str_id.split(":")
        domain_part = "0000" + domain_part if len(domain_part) == 4 else domain_part
        return ":".join([domain_part] + others)

    nb_devices = _cu_check(cuda_driver.cuDeviceGetCount())
    return {GpuCudaId(cuda_id): GpuPciId(get_pci_id(cuda_id)) for cuda_id in range(nb_devices)}
